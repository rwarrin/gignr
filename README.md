# GIGNR

## About
GIGNR is a command line utility to quickly and easily generate .gitignore files
using the [gitignore.io](https://www.gitignore.io) service.

## Usage
Run the `gignr` app and pass it the names of things to generate git ignore files
for. For example to generate a git ignore file for VisualStudio and C you would
type `gignr visualstudio c`. The contents of the ignore file are printed to
standard out so to save the result to a file just type `gignr visualstudio c >
.gitignore`.

## Building
To build on windows open a command prompt and navigate to the misc directory and
run the shell.bat script, this will enable the use of the visual studio compiler
from the command line. Next type `build` and the project will build into the
build directory.

To build on linux execute `make` from the code directory. The build will be
placed in the build directory.

## Installing
Add the program to your system path so you can run it from anywhere.

