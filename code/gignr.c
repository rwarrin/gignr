#include "gignr_platform.h"
#include "gignr.h"

int StrLen(char *str)
{
	char *end = str;
	while(*end != 0)
	{
		++end;
	}

	int Result = (end - str);
	return Result;
}

void StrnAppend(char *dest, int length, char *src)
{
	int strlen = StrLen(dest);
	char *start = dest + (strlen);

	if((start - dest < length) &&
	   (strlen + StrLen(src) < length))
	{
		while(*src != 0)
		{
			*start++ = *src++;
		}
	}
}

int32 StrCmp(char *to, char *from)
{
	for(; *to == *from; ++to, ++from)
	{
		if(*to == 0)
		{
			return 0;
		}
	}

	return *to - *from;
}

void
PrintHelp(char *AppPath)
{
	printf("Usage: %s [options] <ignore types>\n", AppPath);
	printf("\t-help:\tPrints this help message.\n");
	printf("\t-list:\tPrints the list of ignore types available.\n");
	printf("\nIgnore types: space separated list of ignore types to use to generate the .gitignore file.\n");
}

bool32
GetUrl(struct net_url *Result, struct command_line *Commands)
{
	if(Commands->Count == 1)
	{
		PrintHelp(Commands->Arguments[0]);
		return 0;
	}

	StrnAppend(Result->Url, 256, "https://www.gitignore.io/api/");

	for(int index = 1; index < Commands->Count; ++index)
	{
		if(StrCmp(Commands->Arguments[index], "-help") == 0)
		{
			PrintHelp(Commands->Arguments[0]);
			return 0;
		}
		else if(StrCmp(Commands->Arguments[index], "-list") == 0)
		{
			char *NewUrl = "https://www.gitignore.io/api/list/";
			Result->Url[0] = 0;
			StrnAppend(Result->Url, MAX_NETURL_LENGTH, NewUrl);
			Result->Url[StrLen(NewUrl)] = 0;
			return 1;
		}

		StrnAppend(Result->Url, MAX_NETURL_LENGTH, Commands->Arguments[index]);
		if(index != Commands->Count - 1)
		{
			StrnAppend(Result->Url, MAX_NETURL_LENGTH, "%2C");
		}
	}

	return 1;
}

