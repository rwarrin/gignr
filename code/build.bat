@echo off

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

cl /nologo /Oxi -FC -Z7 ..\code\win32_gignr.c /link -INCREMENTAL:NO user32.lib wininet.lib

popd
@echo on

