#ifndef GIGNR_H
#define GIGNR_H

#define KiloBytes(count) (count * 1024)
#define MegaBytes(count) (KiloBytes(count) * 1024)

#define MAX_DOWNLOAD_SIZE MegaBytes(1)

#define ARRAY_COUNT(array) (sizeof(array) / sizeof((array)[0]))

#define MAX_NETURL_LENGTH 256
struct net_url
{
	char Url[MAX_NETURL_LENGTH];
};

struct net_result
{
	int32 MaxSize;
	int32 Size;
	char *Data;
};

#endif  // GIGNR_H
