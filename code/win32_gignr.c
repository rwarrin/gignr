#include "gignr_platform.h"

#include <windows.h>
#include <wininet.h>

#include "win32_gignr.h"
#include "gignr.c"

NET_GET(get_web_results)
{
	HINTERNET Internet = InternetOpen("Gignr - gitignore builder", INTERNET_OPEN_TYPE_PRECONFIG,
									  NULL, NULL, 0);
	if(!Internet)
	{
		return;
	}

	HINTERNET Connection = InternetOpenUrl(Internet, NetUrl->Url, NULL, (DWORD)-1,
										   INTERNET_FLAG_SECURE, 0);
	if(!Connection)
	{
		InternetCloseHandle(Internet);
		return;
	}

	Result->MaxSize = MAX_DOWNLOAD_SIZE;
	Result->Data = VirtualAlloc(0, MAX_DOWNLOAD_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if(!Result->Data)
	{
		InternetCloseHandle(Connection);
		InternetCloseHandle(Internet);
		return;
	}

	DWORD BytesRead = 0;
	if(!InternetReadFile(Connection, Result->Data, MAX_DOWNLOAD_SIZE, &BytesRead))
	{
		InternetCloseHandle(Connection);
		InternetCloseHandle(Internet);
		return;
	}
	Result->Size = BytesRead;

	InternetCloseHandle(Connection);
	InternetCloseHandle(Internet);
}

int main(int argc, char *argv[])
{
	struct command_line Commands = { argc, argv };
	struct net_url NetUrl = {0};
	struct net_result Result = {0};

	if(!GetUrl(&NetUrl, &Commands))
	{
		return 1;
	}
	get_web_results(&NetUrl, &Result);

	printf("%s", Result.Data);

	VirtualFree(Result.Data, Result.MaxSize, MEM_RELEASE);
	return 0;
}
