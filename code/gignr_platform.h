#ifndef GIGNR_PLATFORM_H
#define GIGNR_PLATFORM_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef int8_t int8;
typedef int8_t int16;
typedef int8_t int32;
typedef float real32;
typedef double real64;
typedef int32_t bool32;

#define global_variable static
#define internal static

struct command_line
{
	int32 Count;
	char **Arguments;
};

#ifdef __cplusplus
}
#endif

#define NET_GET(name) void name(struct net_url *NetUrl, struct net_result *Result)
typedef NET_GET(net_get);

#endif  // GIGNR_PLATFORM_H
